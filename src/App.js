import React,{useState,useCallback} from 'react';
import { BrowserRouter as Router, Route, Redirect, Switch } from 'react-router-dom';

import MainNavigation from './shared/components/Navigation/MainNavigation';
import Users from './user/pages/Users';
import NewPlace from './places/pages/NewPlaces';
import UpdatePlace from './places/pages/UpdatePlace';
import UserPlaces from './places/pages/UserPlaces';
import Auth from './user/pages/Auth';
import {AuthContext} from './shared/context/auth-context';

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [userId, setUserId] = useState(false);

  const login = useCallback((uid) => {
  	setIsLoggedIn(true);
  	setUserId(uid);
  }, []);

  const logout = useCallback(() => {
  	setIsLoggedIn(false);
  	setUserId(null);
  }, []);

  let routes;
  if(isLoggedIn){
  	routes = (
  		<Switch>
		  	<Route path="/" exact>
		  		<Users/>
		  	</Route>
		  	<Route path="/:userId/places" exact>
		  		<UserPlaces />
		  	</Route>
		  	<Route path="/places/new" exact>
		  		<NewPlace />
		  	</Route>
		  	<Route path="/places/:placeId" exact>
		  		<UpdatePlace />
		  	</Route>
		  	<Redirect to="/" />		  	
	  	</Switch>	  		
  	);
  } else {
  	routes = (
  		<Switch>
		  	<Route path="/" exact>
		  		<Users/>
		  	</Route>
		  	<Route path="/:userId/places" exact>
		  		<UserPlaces />
		  	</Route>
		  	<Route path="/auth" exact>
		  		<Auth />
		  	</Route>		  	
	  	</Switch>	  		
  	);
  } 

  return (
  	<AuthContext.Provider value={{isLoggedIn: isLoggedIn, userId: userId, login: login, logout: logout}}>
	  <Router>
	  	<MainNavigation />
	  	<main>
			{routes}
	  	</main>
	  </Router>
  	</AuthContext.Provider>

  );
}

export default App;
