import React from 'react';
import { useParams } from 'react-router-dom';

import Placelist from '../components/PlaceList';

const DUMMY_PLACES = [
	{
		id:'p1',
		title: 'Empire State Building',
		description: 'One of the most famous sky scrapers in the world!',
		imageUrl: 'https://media.timeout.com/images/105629094/750/422/image.jpg',
		address: '20 W 34th St, New York, NY 10001, United States',
		location: {
			lat: 40.7994833,
			lng: -73.9856644
		},
		creator: 'u1'

	},
	{
		id:'p2',
		title: 'Emp. State Building',
		description: 'One of the most famous sky scrapers in the world!',
		imageUrl: 'https://media.timeout.com/images/105629094/750/422/image.jpg',
		address: '20 W 34th St, New York, NY 10001, United States',
		location: {
			lat: 40.7994833,
			lng: -73.9856644
		},
		creator: 'u2'
	}
];



const UserPlaces = () => {
	const userId = useParams().userId;
	const loadedPlaces = DUMMY_PLACES.filter(place => place.creator === userId);
	return <Placelist items={loadedPlaces} />;
};

export default UserPlaces;


